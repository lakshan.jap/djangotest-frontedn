import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const API_URL = 'http://127.0.0.1:8000/employee/details/';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
};

@Injectable({
  providedIn: 'root',
})
export class AppService {
  constructor(private _httpClient: HttpClient) {}

  getData(employeeData) {
    return this._httpClient.get(API_URL, {
      params: {
        first_name: employeeData.first_name,
        last_name: employeeData.last_name,
        branch: employeeData.branch,
        country: employeeData.country,
      },
    });
  }
}
