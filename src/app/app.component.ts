import { Component } from '@angular/core';
import { AppService } from './service/app.service';

interface Employee {
  first_name?;
  last_name?;
  branch?;
  country?;
}
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'django-test';
  employee: Employee = {
    first_name: '',
    last_name: '',
    branch: '',
    country: '',
  };
  results: any;
  resultsEmpty: boolean;
  constructor(private _appService: AppService) {}

  onClickMe() {
    this._appService.getData(this.employee).subscribe((res) => {
      console.log(res);
      this.results = res[`data`];
      
    });
  }
}
